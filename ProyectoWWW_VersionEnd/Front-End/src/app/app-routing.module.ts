import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { MainMenuComponent } from './componentes/main-menu/main-menu.component';
import { MenuGeneralComponent } from './componentes/menu-general/menu-general.component';
import { MenuCreacionComponent } from './componentes/menu-creacion/menu-creacion.component';
import { FinancieroComponent } from './componentes/financiero/financiero.component';
import { ClientesComponent } from './componentes/clientes/clientes.component';
import { InternoComponent } from './componentes/interno/interno.component';
import { AprendizajeComponent } from './componentes/aprendizaje/aprendizaje.component';

const routes: Routes = [
	{ path: '', component : LoginComponent},
	{ path : 'login', component : LoginComponent},
	{ path : 'main', component : MainMenuComponent},
	{ path : 'financiero', component: FinancieroComponent},
	{ path : 'clientes', component: ClientesComponent},
	{ path : 'interno', component: InternoComponent},
	{ path : 'aprendizaje', component: AprendizajeComponent},
	{ path : 'creacion', component : MenuCreacionComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
