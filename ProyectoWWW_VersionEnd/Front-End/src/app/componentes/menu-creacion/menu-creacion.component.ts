import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Indicador } from '../../modelo/indicador';
import { FinancieroService } from '../../servicios/financiero.service';

@Component({
  selector: 'app-menu-creacion',
  templateUrl: './menu-creacion.component.html',
  styleUrls: ['./menu-creacion.component.css']
})
export class MenuCreacionComponent implements OnInit {

   @Input()
    customTitle: string;

    @Input()
    cant_datos: number;

    @Input()
    modelo: any;

    boton : string = 'Crear';

	indicadorForm : FormGroup;
	dato : Array<Indicador>;
	texto : string;

  constructor(private formB : FormBuilder,
              private _fS : FinancieroService
            ) {

  	this.dato = [];
  	this.texto = '';
   }

  ngOnInit() {
  	this.initForm();
  	this.onChanges();

  	this.f.get('NombreObjetivo').setValue('');
    console.log(this.customTitle);

    console.log(this.modelo);

  }

  crear(){	


    if(this.customTitle == 'Indicadores de Clientes'){
      this._fS.addInfoCliente(this.f.value).subscribe(res=>{
          console.log(res.message);

          if(res.code == 200){
            if(this.f.get('id').value == ''){
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' creado correctamente';
          this.cant_datos++;
          }else{
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' actualizado correctamente';
          }
          this.resetear();
          }

      },error=>{
        console.log(error);
      });
    }else if(this.customTitle == 'Indicadores Financieros'){
      this._fS.addInfo(this.f.value).subscribe(res=>{
          console.log(res.message);

          if(res.code == 200){
            if(this.f.get('id').value == ''){
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' creado correctamente';
          this.cant_datos++;
          }else{
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' actualizado correctamente';
          }
          this.resetear();
          }

      },error=>{
        console.log(error);
      });
    }else if(this.customTitle == 'Indicadores de Innovacion'){
      this._fS.addInfoInnovacion(this.f.value).subscribe(res=>{
          console.log(res.message);

          if(res.code == 200){
            if(this.f.get('id').value == ''){
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' creado correctamente';
          this.cant_datos++;
          }else{
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' actualizado correctamente';
          }
          this.resetear();
          }

      },error=>{
        console.log(error);
      });
    }else{
      this._fS.addInfoGestionH(this.f.value).subscribe(res=>{
          console.log(res.message);

          if(res.code == 200){
            if(this.f.get('id').value == ''){
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' creado correctamente';
          this.cant_datos++;
          }else{
          this.texto = 'Objetivo '+this.f.get('NombreObjetivo').value+' actualizado correctamente';
          }
          this.resetear();
          }

      },error=>{
        console.log(error);
      });
    }
  	}

  datosForm(datos){
    this.f.setValue(datos);
    this.boton = 'Actualizar';
    this.texto = '';
  }


 	initForm(){
 		this.indicadorForm = this.formB.group({
      id : [''],
 			Id_Usuario : [''+localStorage.getItem('id')],
 			NombreObjetivo : ['', Validators.required],
 			Indicador : ['', Validators.required],
 			Meta : ['', Validators.required],
 			Iniciativa : ['', Validators.required]
 		});

     this.texto = '';
     this.boton = 'Crear';
     this.onChanges();
 	}

 	onChanges(){
 		this.indicadorForm.get('NombreObjetivo').valueChanges.subscribe(
 				valor=>{
 					if(valor == ''){

 						this.indicadorForm.get('Indicador').disable();
 						this.indicadorForm.get('Meta').disable();
 						this.indicadorForm.get('Iniciativa').disable();
 						
 					}else{
 						this.indicadorForm.get('Indicador').enable();
 						this.indicadorForm.get('Meta').enable();
 						this.indicadorForm.get('Iniciativa').enable();
 					}
 				}
 			);
 	}

 	get f() {
 		return this.indicadorForm;
 	}

 	resetear(){
 			this.indicadorForm.reset();
      this.f.controls['Id_Usuario'].setValue(localStorage.getItem('id'));
      this.f.controls['id'].setValue('');
 			this.indicadorForm.get('Indicador').disable();
 			this.indicadorForm.get('Meta').disable();
 			this.indicadorForm.get('Iniciativa').disable();

 	}

}
