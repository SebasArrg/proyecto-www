<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    
    protected $table = 'goals';

    protected $fillable = [ 'descripcion','id_objective'];

    public function objective()
    {
        return $this->hasOne('App\Objective',
        'id','id_objective');
    }

}
