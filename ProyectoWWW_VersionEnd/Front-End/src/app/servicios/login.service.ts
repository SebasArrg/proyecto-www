import { Injectable } from '@angular/core';
import { Http,Headers,Response} from '@angular/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

	url : string;


  constructor(
  	private _http : Http
  	) {
  	this.url = 'http://localhost/derroche/back-end-derroche/index.php/';

  	 }


  	 login(user){
  	 	return this._http.get(this.url+'login/'+user).pipe(map(res=>res.json()));
  	 }



}
