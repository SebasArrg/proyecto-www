-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2019 a las 02:07:10
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `umbrella`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `Id_Cliente` int(10) UNSIGNED NOT NULL,
  `Id_Usuario` int(10) UNSIGNED NOT NULL,
  `NombreObjetivo` varchar(45) DEFAULT NULL,
  `Indicador` int(10) UNSIGNED DEFAULT NULL,
  `Meta` varchar(45) DEFAULT NULL,
  `Iniciativa` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`Id_Cliente`, `Id_Usuario`, `NombreObjetivo`, `Indicador`, `Meta`, `Iniciativa`) VALUES
(12, 1664067, 'trabajargoogle', 1000, 'ganarmuchamoneyengoogle', 666);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiera`
--

CREATE TABLE `financiera` (
  `Id_Financiera` int(10) UNSIGNED ZEROFILL NOT NULL,
  `Id_Usuario` int(10) UNSIGNED NOT NULL,
  `NombreObjetivo` varchar(45) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `Indicador` int(10) UNSIGNED DEFAULT NULL,
  `Meta` varchar(45) DEFAULT NULL,
  `Iniciativa` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `financiera`
--

INSERT INTO `financiera` (`Id_Financiera`, `Id_Usuario`, `NombreObjetivo`, `Indicador`, `Meta`, `Iniciativa`) VALUES
(0000054232, 1664067, 'tuhermana', 69, 'darle buenos consejos', 123213),
(0000054233, 1664066, 'algo', 123, 'money', 123),
(0000054234, 3543, 'comer', 0, 'todos', 0),
(0000054235, 3543, 'comer', 2, 'empanadas', 66),
(0000054236, 3543, 'comer', 0, 'nnnnnnnnnnnnnnnnnnnn', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestionhumana`
--

CREATE TABLE `gestionhumana` (
  `ID_GestionHumana` int(10) UNSIGNED NOT NULL,
  `Id_Usuario` int(10) UNSIGNED NOT NULL,
  `NombreObjetivo` varchar(45) DEFAULT NULL,
  `Indicador` int(10) UNSIGNED DEFAULT NULL,
  `Meta` varchar(45) DEFAULT NULL,
  `Iniciativa` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gestionhumana`
--

INSERT INTO `gestionhumana` (`ID_GestionHumana`, `Id_Usuario`, `NombreObjetivo`, `Indicador`, `Meta`, `Iniciativa`) VALUES
(1234, 1664066, 'ganar bien ricolino', 100, 'con un rico cinco', 123213);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `innovacion`
--

CREATE TABLE `innovacion` (
  `Id_Innovacion` int(10) UNSIGNED NOT NULL,
  `Id_Usuario` int(10) UNSIGNED NOT NULL,
  `NombreObjetivo` varchar(45) DEFAULT NULL,
  `Indicador` int(10) UNSIGNED DEFAULT NULL,
  `Meta` varchar(45) DEFAULT NULL,
  `Iniciativa` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `innovacion`
--

INSERT INTO `innovacion` (`Id_Innovacion`, `Id_Usuario`, `NombreObjetivo`, `Indicador`, `Meta`, `Iniciativa`) VALUES
(1, 1664067, 'ganar ', 8989, 'sdfsd', 123213);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idRoles` int(10) UNSIGNED NOT NULL,
  `Usuarios_Id_Usuario` int(10) UNSIGNED NOT NULL,
  `NombreRol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idRoles`, `Usuarios_Id_Usuario`, `NombreRol`) VALUES
(1, 4565, 'Financiero'),
(2, 6787, 'servicioCliente'),
(3, 3543, 'Investigacion&Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id_Usuario` int(10) UNSIGNED NOT NULL,
  `Usuario` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id_Usuario`, `Usuario`, `Clave`) VALUES
(3543, 'Investigacion&Desarrollo', 'investigacion1234'),
(4565, 'Financiero', 'financiero1234'),
(6787, 'ServicioCliente', 'servicio1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`Id_Cliente`),
  ADD KEY `Id_Usuario` (`Id_Usuario`);

--
-- Indices de la tabla `financiera`
--
ALTER TABLE `financiera`
  ADD PRIMARY KEY (`Id_Financiera`),
  ADD KEY `Id_Usuario` (`Id_Usuario`);

--
-- Indices de la tabla `gestionhumana`
--
ALTER TABLE `gestionhumana`
  ADD PRIMARY KEY (`ID_GestionHumana`),
  ADD KEY `Id_Usuario` (`Id_Usuario`);

--
-- Indices de la tabla `innovacion`
--
ALTER TABLE `innovacion`
  ADD PRIMARY KEY (`Id_Innovacion`),
  ADD KEY `Id_Usuario` (`Id_Usuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idRoles`),
  ADD KEY `Usuarios_Id_Usuario` (`Usuarios_Id_Usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id_Usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `Id_Cliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `financiera`
--
ALTER TABLE `financiera`
  MODIFY `Id_Financiera` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54237;

--
-- AUTO_INCREMENT de la tabla `gestionhumana`
--
ALTER TABLE `gestionhumana`
  MODIFY `ID_GestionHumana` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1235;

--
-- AUTO_INCREMENT de la tabla `innovacion`
--
ALTER TABLE `innovacion`
  MODIFY `Id_Innovacion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `idRoles` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id_Usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1664068;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`Id_Usuario`) REFERENCES `usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `financiera`
--
ALTER TABLE `financiera`
  ADD CONSTRAINT `financiera_ibfk_1` FOREIGN KEY (`Id_Usuario`) REFERENCES `usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gestionhumana`
--
ALTER TABLE `gestionhumana`
  ADD CONSTRAINT `gestionhumana_ibfk_1` FOREIGN KEY (`Id_Usuario`) REFERENCES `usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `innovacion`
--
ALTER TABLE `innovacion`
  ADD CONSTRAINT `innovacion_ibfk_1` FOREIGN KEY (`Id_Usuario`) REFERENCES `usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`Usuarios_Id_Usuario`) REFERENCES `usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
