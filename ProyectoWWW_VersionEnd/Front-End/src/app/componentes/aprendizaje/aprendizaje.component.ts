import { Component, OnInit, ViewChild } from '@angular/core';
import { FinancieroService } from '../../servicios/financiero.service';
import { Indicador } from '../../modelo/indicador';
import { MenuCreacionComponent } from '../../componentes/menu-creacion/menu-creacion.component';

@Component({
  selector: 'app-aprendizaje',
  templateUrl: '../menu-general/menu-general.component.html',
  styleUrls: ['./aprendizaje.component.css']
})
export class AprendizajeComponent implements OnInit {

  @ViewChild(MenuCreacionComponent) hijo: MenuCreacionComponent;
  
  modelo : Indicador;

	titulo = 'Indicadores de Gestion Humana';
	datos : Array<any>;
  rol : string = '4';
  rol_user : string;
  constructor(
    private _fS : FinancieroService
    ) {
    this.datos = [];
    this.rol_user = localStorage.getItem('rol');
   }

  ngOnInit() {
  	this.getData();
  }

    getData(){
  	  	this._fS.getInfoGestionH().subscribe(res=>{

  		if(res.code == 200){

        this.datos = [];

  			for (var i = 0; i < res.data.length; ++i) {
  			this.datos.push(res.data[i]);	
  			}
  		}

  	},error=>{
  		console.log(error);
  	});
  }


  delete(id){
    console.log(id);
    this._fS.DeleteInfoGestionH(id).subscribe(res=>{

      if(res.code == 200)
        console.log('OK');
        this.getData();
    },error=>{
      console.log(error);
    });
  }

  edit(id, objetivo, indicador, meta, iniciativa){
    this.modelo = {
      id : id,
      Id_Usuario : localStorage.getItem('id'),
      NombreObjetivo : objetivo,
      Indicador : indicador,
      Meta : meta,
      Iniciativa : iniciativa
    }

   this.hijo.datosForm(this.modelo);
  }

  open(){
    this.hijo.initForm();
  }

}
