<?php

namespace App\Http\Controllers;

use App\Initiative;
use App\Objective;
use Illuminate\Http\Request;

class InitiativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Initiative::all();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Initiative::create($request->all());

        return response()->json
        ([
            "La iniciativa ha sido creada correctamente"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Initiative  $initiative
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objetivo = Objective::find($id);
        return $objetivo->Initiatives;
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Initiative  $initiative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $initiative = Initiative::find($id);
        $descripcionInic = $request->input('descripcion');

        $initiative->descripcion = $descripcionInic;
        $initiative->save();
        
        return response()->json
        ([
            "La iniciativa fue modificada correctamente"
        ], 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Initiative  $initiative
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $initiative = Initiative::find($id);

        $initiative->delete();

        return response()->json 
        ([
            "La iniciativa fue eliminado correctamente"
        ], 200);


    }
}
