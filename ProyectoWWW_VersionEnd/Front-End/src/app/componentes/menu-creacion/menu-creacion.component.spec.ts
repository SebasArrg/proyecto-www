import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCreacionComponent } from './menu-creacion.component';

describe('MenuCreacionComponent', () => {
  let component: MenuCreacionComponent;
  let fixture: ComponentFixture<MenuCreacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCreacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCreacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
