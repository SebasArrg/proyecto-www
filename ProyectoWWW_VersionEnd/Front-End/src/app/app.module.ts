import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/login/login.component';
import { MainMenuComponent } from './componentes/main-menu/main-menu.component';
import { MenuGeneralComponent } from './componentes/menu-general/menu-general.component';
import { MenuCreacionComponent } from './componentes/menu-creacion/menu-creacion.component';
import { FinancieroComponent } from './componentes/financiero/financiero.component';
import { ClientesComponent } from './componentes/clientes/clientes.component';
import { InternoComponent } from './componentes/interno/interno.component';
import { AprendizajeComponent } from './componentes/aprendizaje/aprendizaje.component';
import { ConfiguracionEmpresaComponent } from './componentes/configuracion-empresa/configuracion-empresa.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainMenuComponent,
    MenuGeneralComponent,
    MenuCreacionComponent,
    FinancieroComponent,
    ClientesComponent,
    InternoComponent,
    AprendizajeComponent,
    ConfiguracionEmpresaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
