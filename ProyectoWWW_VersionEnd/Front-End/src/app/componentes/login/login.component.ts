import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../servicios/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  mensaje_info : string = '';
  constructor(
  	private ruta : Router,
    private formBuilder : FormBuilder,
    private _lS : LoginService
  	) {
  }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      user: ['', Validators.required],
      pass: ['', Validators.required]
    });


  }

  onSubmit(user, pass){


    this._lS.login(user).subscribe(
            response=>{

         console.log(response);

        if(response.data.length > 0){

        if(response.data[0].Usuario == user && response.data[0].Clave == pass){

          localStorage.setItem('id', response.data[0].Id_Usuario);
          localStorage.setItem('rol', response.data[0].idRoles);
          
        this.ruta.navigate(['/main']);


        }else if(response.data[0].Usuario == user && response.data[0].Clave != pass){
          this.mensaje_info = 'Clave incorrecta';
        }
      }else{
        this.mensaje_info = 'Usuario no existe';
      }

      },error=>{
        this.mensaje_info = error;
      }
      );

    return false;




  }

}
