<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Initiative extends Model
{
    protected $table = 'initiatives';

    protected $fillable = [ 'descripcion','id_objective'];

    public function objective()
    {
        return $this->hasOne('App\Objective',
        'id','id_objective');
    }
}
