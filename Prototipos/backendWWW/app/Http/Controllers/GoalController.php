<?php

namespace App\Http\Controllers;

use App\Goal;
use App\Objective;
use Illuminate\Http\Request;

class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Goal::all();

        
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
         Goal::create($request->all());

        return response()->json([
            "Meta Creada Correctamente."
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objetivo = Objective::find($id);
        return $objetivo->Goals;
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $goal = Goal::find($id);

        $descripcionMeta = $request->input('descripcion');
        


        $goal->descripcion = $descripcionMeta;
        
        $goal->save();

        return response()->json([
               
		 "Meta Modificada Correctamente."
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $goal = Goal::find($id);
       
       $goal->delete();

       return response()->json
            ([
                "Meta Eliminada Correctamente."
            ], 200);

    }
}
