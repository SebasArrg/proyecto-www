import { Injectable } from '@angular/core';
import { Http,Headers,Response} from '@angular/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Indicador } from '../modelo/indicador';

@Injectable({
  providedIn: 'root'
})
export class FinancieroService {

url = 'http://localhost/derroche/back-end-derroche/index.php/';	

  constructor(
  	private _http : Http
  )
  { }

  //Obtener datos
  public getInfo(){
  	return this._http.get(this.url+'financiero').pipe(map(res=>res.json()));
  }

  public getInfoClientes(){
    return this._http.get(this.url+'clientes').pipe(map(res=>res.json()));
  }

  public getInfoGestionH(){
    return this._http.get(this.url+'gestionhumana').pipe(map(res=>res.json()));
  }

  public getInfoInnovacion(){
    return this._http.get(this.url+'innovacion').pipe(map(res=>res.json()));
  }

  //Agregar Datos
  public addInfo(data : Indicador){

  let json = JSON.stringify(data);
	let params = 'json='+json;
	let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

	return this._http.post(this.url+'registro-financiera', params, {headers: headers})
						 .pipe(map(res => res.json()));
  }


  public addInfoCliente(data : Indicador){

  let json = JSON.stringify(data);
  let params = 'json='+json;
  let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

  return this._http.post(this.url+'registro-cliente', params, {headers: headers})
             .pipe(map(res => res.json()));
  }


  public addInfoGestionH(data : Indicador){

  let json = JSON.stringify(data);
  let params = 'json='+json;
  let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

  return this._http.post(this.url+'registro-gestionhumana', params, {headers: headers})
             .pipe(map(res => res.json()));
  }

  public addInfoInnovacion(data : Indicador){

  let json = JSON.stringify(data);
  let params = 'json='+json;
  let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

  return this._http.post(this.url+'registro-innovacion', params, {headers: headers})
             .pipe(map(res => res.json()));
  }

  //Borrar Datos

  public DeleteInfo(id){
    return this._http.get(this.url+'financiero/'+id).pipe(map(res=>res.json()));
  }

  public DeleteInfoCliente(id){
    return this._http.get(this.url+'clientes/'+id).pipe(map(res=>res.json()));
  }

  public DeleteInfoInnovacion(id){
    return this._http.get(this.url+'innovacion/'+id).pipe(map(res=>res.json()));
  }

  public DeleteInfoGestionH(id){
    return this._http.get(this.url+'gestionhumana/'+id).pipe(map(res=>res.json()));
  }



}
