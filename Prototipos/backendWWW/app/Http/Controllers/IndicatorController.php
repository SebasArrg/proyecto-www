<?php

namespace App\Http\Controllers;

use App\Indicator;
use App\Objective;
use Illuminate\Http\Request;

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Indicator::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Indicator::create($request->all());

        return response()->json
        ([
            "El indicador fue creado Correctacmente."
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objetivo = Objective::find($id);
        return $objetivo->Indicators;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indicator = Indicator::find($id);
        
        $descripcionIndi = $request->input('descripcion');

        $indicator->descripcion = $descripcionIndi;
        $indicator->save();

        return response()->json
        ([
            "El indicador fue modificado correctamente"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Indicator  $indicator
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indicator = Indicator::find($id);

        $indicator->delete();

        return response()->json 
        ([
            "El indicador fue eliminado correctamente"
        ], 200);
    }
}
