<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

require 'vendor/autoload.php';

$app = new \Slim\Slim();


$db = new mysqli("localhost","root","","umbrella");

require "modelos/clientes/clientes.php";
require "modelos/financiero/financiero.php";
require "modelos/gestionHumana/gestionh.php";
require "modelos/innovacion/innovacion.php";
require "modelos/logueo/logueo.php";

$app->run();