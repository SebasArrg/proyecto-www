//alert ('entre');

function ajaxGet(url, callback) {
  
    var req = new XMLHttpRequest();
   
    req.open("GET", url, true);
    req.addEventListener("load", function() {
      if (req.status >= 200 && req.status < 400) {
        // Llamada ala función callback pasándole la respuesta
        callback(req.responseText);
      } else {
        console.error(req.status + " " + req.statusText);
      }
    });
    req.addEventListener("error", function(){
      console.error("Error de red");
    });
    req.send(null);
  }

  function mostrar(respuesta) {
    console.log(respuesta);
}

ajaxGet("http://127.0.0.1:8000/users", mostrar);
ajaxGet("http://127.0.0.1:8000/users", function(respuesta) {
  console.log(respuesta);
});