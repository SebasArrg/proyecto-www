<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
	protected $table = 'objetivos';

    protected $fillable = [
        'nombre', 'id_rol'
    ];

    public function Goals()
    {
        return $this->hasMany('App\Goal',
            'id_objective');
    }

    public function Indicators()
    {
        return $this->hasMany('App\Indicator',
            'id_objective');
    }

    public function Initiatives()
    {
        return $this->hasMany('App\Initiative',
            'id_objective');
    }

    public function Role()
    {
        return $this->hasOne('App\Role',
        'id','id_rol');
    }
}
